# resist.guide
Neither the mentor we needed,  
nor the guidepost we sought,  
but what we deduced from what was found among the rubble.

# todo
- Add a code of conduct.
- Add documentation for contributing and creating issues.
- Add documentation for setting up a development environment.
- Figure out why HTTPS breaks when setting CloudFlare to "Strict" mode.

# thanks
Special thanks is extended to the following organizations and individuals:
- [Alexandre Vicenzi](http://www.alexandrevicenzi.com) for the [Flex theme for Pelican](https://github.com/alexandrevicenzi/Flex).  
- [Eliot Berriot](https://eliotberriot.com/) for their [Intro to Git](https://eliotberriot.com//blog/2019/03/05/building-sotfware-together-with-git/).  
- [Tyler](http://beneathdata.com/how-to/how-i-built-this-website/). [Vaneyckt](https://vaneyckt.io/posts/safer_bash_scripts_with_set_euxo_pipefail/), [Danluu](https://danluu.com/cli-complexity/), and [Cyrille](https://cyrille.rossant.net/pelican-github/) for shared knowledge.  
- [Amnesty International](https://www.amnesty.org/) for their [SafetyDuringProtest](https://www.amnestyusa.org/pdfs/SafeyDuringProtest_F.pdf) pamphlet.  
- [GitHub](https://github.com/) and [gitea](https://gitea.com/) for making open source projects possible.  
- BrowserStack for their [enthusiastic support of open source](https://www.browserstack.com/open-source).  
- [Portland ISO](https://www.portlandsocialists.org/) and [GenStrike2020](https://genstrike.org/) for peacefully organizing collective action.  
- The many members of the [Pelican](https://blog.getpelican.com/) project.  
- [Wikipedia](https://www.wikipedia.org/) for the [solidarity fist logo](https://commons.wikimedia.org/wiki/File:Fist.svg).  
- Last but not least, the entire open source community for providing the tools and knowledge to freely make this project.

# online

See [resist.guide](https://resist.guide/) or the [archive](https://archive.ph/resist.guide) for the live view.

# license
Licensed under the [Creative Commons Attribution-NonCommercial-ShareAlike 4.0 International](https://creativecommons.org/licenses/by-nc-sa/4.0/) license.

[Markdown formatted text of the license](https://github.com/idleberg/Creative-Commons-Markdown/blob/aa8f8a69984eb15ec7657cc8c2db995df1b49309/4.0/by-nc-sa.markdown) obtained from the [Creative Commons Markdown](https://github.com/idleberg/Creative-Commons-Markdown) repo on Github.
