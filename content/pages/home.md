title: We of Resist(dot)Guide...
authors: jakimfett
summary: A resistance guide for modern activists.
status: hidden
save_as: index.html

...support emergent localized rent, debt, and labour strikes,  
as well as the [organized general strike](https://genstrike.org/).

---
## We of resist.guide...
...are creating accessible tools,  
to meet the current and ongoing needs of individuals coordinating mutual aid.  


---

If you [grok](http://catb.org/jargon/html/G/grok.html) git [version control](https://en.wikipedia.org/wiki/Version_control), or are [willing to learn](https://eliotberriot.com//blog/2019/03/05/building-sotfware-together-with-git/), we could use your mind and skills on our team.

See "contribute", via the sidebar.
