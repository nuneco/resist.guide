title: Prepare
category: Activism
tags: checklists
slug: prepare
authors: jakimfett
summary: Using our privilege to uplift the marginalized, through planning and preparation preceeding action.

Communication is a core need of organization.

Development happens via git and [gitea](https://gitea.com/nuneco/),  
everyday discussion happens on Discord (unfortunately),  
and everything else is an [email](mailto:&#106;&#097;&#107;&#105;&#109;&#102;&#101;&#116;&#116;&#064;&#103;&#109;&#097;&#105;&#108;&#046;&#099;&#111;&#109;).


In an ideal world,  
we'd reach through the isolation  
to help one another out.  

This [printables directory](https://drive.google.com/drive/folders/1pWe21PTXX55cxU5fgd47imXRH0ggUWHS) may help with that.

---

In this world,  
we have some partial solutions,  
but none that quite work "well".

If you already have a secure (or efficient) workflow,  
let us know, and we'll adjust.

Accessible communication is a core goal.
