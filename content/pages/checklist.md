title: Checklist
category: activism
tags: checklist, mutual aid, event
slug: checklist
authors: jakimfett, sesshirecat
summary: A checklist for short duration activism events.
status: hidden

Activism requires some preparation.

* Backpack or shoulder bag for carrying supplies
* 5-14 days of medications  
* An epi pen (in a sealed container)  
* An antihistamine (in sealed, labeled plastic bag)
* Cell phone extra battery or charger+cord
* Emergency blanket
* First aid kit
* Flashlight
* Granola bars
* Handheld notebook(s)
* Hand sanitizer 
* Ibuprofen (in sealed, labeled plastic bag)
* Paper towels or a couple of rags
* Pen(s)
* Sewing kit
* Sharpies
* Water bottle 
* Wet wipes

Seasonally, you'll want to include:
* Sun/rain hat
* Sunscreen
* Umbrella
* Extra windbreaker/jacket
* Fingerless gloves (if windy or cold)

For westernized society, you may want to include the following:

* $20-$50 in small bills
* Prepaid cell phone
* Distributeables (eg [ACLU foldables](https://shop.aclu.org/product/ACLU-Know-Your-Rights-Cards-Variety-Pack))
* Law enforcement interaction script
* Eye protection (if concerned about chemicals)
* Clean bandanna (or filter mask if concerned about tear gas)
* Clean change of clothing, sealed in an airtight container (if concerned about tear gas)
* Passport/identification/document/cash holder
* Emergency contact info for your legal advisor, if any.

Day-of event prep:  

* Apply sunscreen
* Double check to make sure anything weapon-like is removed.
* Remove all non-essential items from pockets and bags
* Securely store identification and cash (a passport holder can be used)
* Signs/placards/etc prepared
* Use the bathroom
* Write your emergency contact info on your arm (sharpie or pen).
