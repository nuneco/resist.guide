title: Take Action
date: 2020-03-26 14:14
modified: 2020-03-26 16:20
category: organization
tags: activism, resisting
slug: action
authors: jakimfett
summary: Organization, collaboration, and mutual aid resources.
status: draft

Hello, friend.

There's trouble in the wind,  
and we're neighbors,  
of a sort.

We both live on the same network.

Now, we heard (by way of the grape-vine) that you might be in need.  
And, sometimes, expressing need is pretty difficult, especially to strangers.

The Covid-19 outbreak presents an opportunity for us to help one another.
---
We agree that [humans deserve basic rights](https://www.ohchr.org/EN/UDHR/Documents/UDHR_Translations/eng.pdf),  
and humanitarian aid is desparately needed by the people,  
regardless of what big governments are squabbling about these days.

---

A [#GeneralStrike](https://genstrike.org/) means we agree to refuse non-critical requests for labour.

A #GeneralStrike means we respectfully decline to pay rent,  
after first coordinating with our neighbors.

If we act alone, we will be overwhelmed.

We must unite.

---

The premise is simple.

We need systemic freedom, for a while, from debt, labour expectations, and healthcare costs.

We need these for those who have no option.  
We need these for the elderly, limited ability, and vulnerable communities we live among.  
We need these for fairness and equality.  

Each step your neighborhood takes towards cohesive community is a step towards that goal.



Immediate Action & Mutual Aid:  
	* coordinate groceries purchased for your block.  
	* use extra resources and ability to sustain others in your community.  
	* commit to resisting the strain this crisis places on our families.   


Long Term Collective Action:  
	* Organize with [regional mutual aid groups](https://itsgoingdown.org/c19-mutual-aid/), or form your own.  

Followthrough and routine are essential in collective action.

Make mutual aid part of your schedule today, and every moment after.

---
And,

When trouble rises,  
Stand with your neighbor  
rather than the employer or the state.

Call for the end of for-profit medical institutuions.  
Call for the end of for-profit lawmaking.  
Call for the end of for-profit war.

If we cannot help our neighbor rebuild,  
are we able to call ourselves a community?
