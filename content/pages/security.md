title: Security
date: 2018-03-27 14:24
modified: 2018-03-27 14:30
category: opsec
tags: security
slug: security
authors: jakimfett
summary: Dead activists needed better security. Embrace strong security practices, or die. Literally. They'll find you, and kill you.
status: draft

There's a myth that says gaining knowledge must be a struggle.  

This myth is a lie.  

Gaining knowledge is as simple as listening.
The quieter you are, the more you can hear.

Operational Security, or `opsec`, is the difference between a successful and an unsuccessful team.
When your enemy can access your communications, it doesn't matter how good your plan was.

There are a lot of good tools out there.
Some of them are better than others.

But they are just tools, and they *cannot* replace a careful mindset.
Filter what you are told. Ask questions and determine truth.

Plans built on false or partial information, will fail.
Plans that do not adapt, will fail.
Plans that are leaked, will fail.

If you want to be an activist, start by securing your things, your data, and your thoughts.


Now.  
Let's talk about some good tools.